#include <ctype.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "shell.h"
#include "shellmemory.h"

int is_directory(const char* path);
int is_file(const char* path);
int interpreter(char* command_args[], int args_size);
int help();
int quit();
int set(char* var, char* value);
int print(char* var);
int run(char* script);
int badcommand();
int badcommand_function(char* function_name);
int badcommand_file_does_not_exist();
int echo(char* var);
int sys_command(char* command, char* arguments);
int count_tokens(const char* str);
int my_ls();
int my_mkdir(char* dirname);
int my_touch(char* filename);
int my_cd(char* dirname);
int my_cat(char* filename);
int variable_exists(char* var);

// Display an error message for unknown commands
int badcommand() {
  printf("%s\n", "Unknown Command");
  return 1;
}
// Display an error message for functions with bad arguments
int badcommand_function(char* function_name) {
  printf("Bad command: %s\n", function_name);
  return 1;
}

// Display an error message for file not found during the 'run' command
int badcommand_file_does_not_exist() {
  printf("%s\n", "Bad command: File not found");
  return 3;
}

/**
 * Function to check whether a directory exists or not.
 * It returns 1 if the directory exists at given path
 * otherwise it returns 0.
 */
int is_directory(const char* path) {
  DIR* dir = opendir(path);
  if (dir != NULL) {
    closedir(dir);
    return 1;  // It's a directory
  }
  return 0;  // Not a directory or does not exist
}

/**
 * Function to check whether a file exists or not.
 * It returns 1 if the file exists at given path
 * otherwise returns 0.
 */
int is_file(const char* path) {  // Check if it's a file, not a directory
  if (is_directory(path)) {
    return 0;
  }
  // Try to open the file
  FILE* fptr = fopen(path, "r");

  // If file does not exists
  if (fptr == NULL) return 0;

  // File exists hence close file and return true.
  fclose(fptr);
  return 1;
}

// Interpret commands and their arguments
int interpreter(char* command_args[], int args_size) {
  int i;

  if (args_size < 1) {
    return badcommand();
  }
  for (i = 0; i < args_size; i++) {  // strip spaces, new lines
    command_args[i][strcspn(command_args[i], "\r\n")] = 0;
  }

  if (strcmp(command_args[0], "help") == 0) {
    // help
    if (args_size != 1) return badcommand();
    return help();

  } else if (strcmp(command_args[0], "quit") == 0) {
    // quit
    if (args_size != 1) return badcommand();
    return quit();

  } else if (strcmp(command_args[0], "set") == 0) {
    // Calculate total length needed for concatenated value
    int total_length = 0;
    for (int i = 2; i < args_size; i++) {
      total_length += strlen(command_args[i]) + 1;  // +1 for space or null terminator
    }

    // Allocate memory for concatenated value
    char* value = (char*)malloc(total_length * sizeof(char));
    if (value == NULL) {
      return badcommand_function("set");
    }

    value[0] = '\0';  // Ensure it's a null-terminated string

    // Concatenate arguments
    for (int i = 2; i < args_size; i++) {
      strcat(value, command_args[i]);
      if (i < args_size - 1) {
        strcat(value, " ");
      }
    }  // command_args[1] is the variable name, value is the alphanumeric tokens
    return set(command_args[1], value);
  } else if (strcmp(command_args[0], "print") == 0) {
    if (args_size != 2) return badcommand();
    return print(command_args[1]);
  } else if (strcmp(command_args[0], "run") == 0) {
    if (args_size != 2) return badcommand();
    return run(command_args[1]);

  } else if (strcmp(command_args[0], "echo") == 0) {
    if (args_size != 2) return badcommand_function("echo");
    return echo(command_args[1]);
  } else if (strcmp(command_args[0], "my_ls") == 0) {
    if (args_size != 1) {
      return badcommand_function("my_ls");
    }
    return my_ls();
  } else if (strcmp(command_args[0], "my_mkdir") == 0) {
    if (args_size != 2) return badcommand_function("my_mkdir");
    return my_mkdir(command_args[1]);
  } else if (strcmp(command_args[0], "my_touch") == 0) {
    if (args_size != 2) return badcommand_function("my_touch");
    return my_touch(command_args[1]);
  } else if (strcmp(command_args[0], "my_cd") == 0) {
    if (args_size != 2) return badcommand_function("my_cd");
    return my_cd(command_args[1]);
  } else if (strcmp(command_args[0], "my_cat") == 0) {
    if (args_size != 2 || is_file(command_args[1]) == 0) {
      return badcommand_function("my_cat");
    }
    return my_cat(command_args[1]);
  } else
    return badcommand();
}

// Display help information for available commands
int help() {
  char help_string[] =
      "COMMAND			DESCRIPTION\n \
	help			Displays all the commands\n \
	quit			Exits / terminates the shell with “Bye!”\n \
	set VAR STRING		Assigns a value to shell memory\n \
	print VAR		Displays the STRING assigned to VAR\n \
	run SCRIPT.TXT		Executes the file SCRIPT.TXT\n ";
  printf("%s\n", help_string);
  fflush(stdout);
  return 0;
}
// Quit the shell with a goodbye message
int quit() {
  printf("%s\n", "Bye!");
  exit(0);
}

/* Counts the number of tokens in the input string and returns the count of tokens
 */
int count_tokens(const char* str) {
  int count = 0;
  int in_token = 0;
  while (*str) {
    if (*str == ' ') {
      in_token = 0;
    } else if (in_token == 0) {
      in_token = 1;
      count++;
    }
    str++;
  }
  return count;
}

// Set the value of a shell variable, ensuring valid variable name and token count
int set(char* var, char* value) {
  // Check if the variable name is empty
  if (var == NULL || var[0] == '\0') {
    return badcommand_function("set");
  }
  int token_count = count_tokens(value);
  //  set command must contain between 1 and 5 alphanumeric tokens
  if (token_count < 1 || token_count > 5) {
    return badcommand_function("set");
  }
  mem_set_value(var, value);
  return 0;  // Returning 0 to indicate success
}

// Display strings or variable values based on the input for the 'echo' command
int echo(char* var) {
  // Check if var is null/empty
  if (var == NULL || var[0] == '\0') {
    return badcommand_function("echo");
  }
  // Check if the input is a variable
  if (var[0] == '$') {
    // Skip the first character which is $
    char* var_name = var + 1;
    if (variable_exists(var_name)) {
      print(var_name);
    } else {
      printf("\n");
    }
  }  // Input is just a string, not a variable
  else {
    printf("%s\n", var);
  }
}
// Check if a variable with the given name exists in shell memory
int variable_exists(char* var) {
  char* mem_value = mem_get_value(var);
  return strcmp(mem_value, "Variable does not exist");
}
// Print the value associated with the given variable
int print(char* var) {
  printf("%s\n", mem_get_value(var));
  return 0;
}

// Change the current working directory to the specified directory
int my_cd(char* dirname) {
  // Attempt to change the directory
  if (chdir(dirname) != 0) {
    // If chdir fails, print an error message and return 1
    return badcommand_function("my_cd");
  }
  return 0;  // Return 0 to indicate success
}

// Run a script file by interpreting and executing each line
int run(char* script) {
  int errCode = 0;
  char line[1000];
  FILE* p = fopen(script, "rt");  // the program is in a file

  if (p == NULL) {
    return badcommand_file_does_not_exist();
  }

  fgets(line, 999, p);
  while (1) {
    errCode = parseInput(line);  // which calls interpreter()
    memset(line, 0, sizeof(line));

    if (feof(p)) {
      break;
    }
    fgets(line, 999, p);
  }

  fclose(p);

  return errCode;
}

// Execute a system command with specified arguments
int sys_command(char* command, char* arguments) {
  // Calculate the size needed for the command string
  int command_size = snprintf(NULL, 0, "%s %s", command, arguments) +1;  // +1 for null terminator

  // Allocate memory for the command string
  char* full_command = malloc(command_size);
  if (full_command == NULL) {
    fprintf(stderr, "Memory allocation failed.\n");
    return 1;  // Return error
  }

  // Create the command string
  snprintf(full_command, command_size, "%s %s", command, arguments);

  fflush(stdout);

  // Execute the command
  int result = system(full_command);

  // Free the allocated memory
  free(full_command);

  return result;
}

// Execute the 'ls' command to list files in the current directory
int my_ls() {
  char* dirname = ".";
  return sys_command("ls", dirname);
}

// Execute the 'mkdir' command to create a new directory
int my_mkdir(char* dirname) {
	return sys_command("mkdir", dirname);
}

// Execute the 'touch' command to create an empty file
int my_touch(char* filename) {
	return sys_command("touch", filename);
}

// Execute the 'cat' command to display the contents of a file
int my_cat(char* filename) {
	return sys_command("cat", filename);
}
